﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using dbwt_m3_mvc.Models;

namespace dbwt_m3_mvc.Controllers
{
    public class ProduktController : Controller
    {
        // GET: Produkt
        public ActionResult Index() {
            ViewBag.Kategorie = "Bestseller";
            if (int.TryParse(Request.Params["category"], out int catId) && catId > 0) {
                Kategorie kat = Kategorie.GetById(catId);
                if (kat != null)
                    ViewBag.Kategorie = kat.Bezeichnung;
                else
                    catId = 0;
            }
            else
                catId = 0;

            if (!int.TryParse(Request.Params["limit"], out int limit) || limit <= 0)
                limit = 8;

            ViewBag.Kategorien = Kategorie.GetList();
            ViewBag.Produkte = Produkt.GetList(catId, limit);
            return View();
        }

       public ActionResult Detail(int id) {
            string role = "gast";
            if (!string.IsNullOrEmpty(Session["role"] as string))
                role = Session["role"].ToString();
            Produkt produkt = Produkt.GetByID(id);
            if (produkt != null)
                return View(produkt);
            return Redirect("Index");
        }
    }
}