﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using dbwt_m3_mvc.Models;

namespace dbwt_m3_mvc.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            if (!string.IsNullOrEmpty(Session["user"] as string)) {
                return Redirect("LogoutForm");
            }
            return Redirect("LoginForm");
        }

        public ActionResult LoginForm()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoginForm(FormCollection form)
        {
            if (string.IsNullOrEmpty(form["loginname"]) || string.IsNullOrEmpty(form["password"])) {
                if (string.IsNullOrEmpty(form["loginname"]))
                    ModelState.AddModelError("loginname", "Bitte Benutzernamen eingeben");
                if (string.IsNullOrEmpty(form["password"]))
                    ModelState.AddModelError("password", "Bitte Passwort eingeben");

                return View();
            }

            else {
                FENutzer user = FENutzer.GetByCredentials(form["loginname"], form["password"]);
                if (user != null) {
                    Session["user"] = user.Vorname;
                    Session["role"] = user.Role;

                    if (user.LetzterLogin.CompareTo(new DateTime(1, 1, 1, 1, 1, 1)) > 0) 
                        TempData["flash"] = "Hallo " + user.Vorname + ", Sie sind angemeldet als " + user.Role + " und waren zuletzt am " + user.LetzterLogin + " bei uns!";
                    else
                        TempData["flash"] = "Hallo " + user.Vorname + ", Sie sind angemeldet als " + user.Role + " und besuchen uns zum ersten Mal. Herzlich Willkommen!";
                }
                else
                    TempData["flash"] = "Benutzername oder Passwort falsch, oder das Konto wurde noch nicht aktiviert";
            }
            if (Request.UrlReferrer.PathAndQuery.Equals("/Login/LoginForm"))
                return RedirectToAction("Index", "Home");
            return Redirect(Request.UrlReferrer.ToString());
        }

        public ActionResult LogoutForm()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LogoutForm(FormCollection form)
        {
            if (!string.IsNullOrEmpty(Session["user"] as string)) {
                Session.Remove("role");
                Session.Remove("user");
                TempData["flash"] = "Sie haben sich erfolgreich abgemeldet";
            }
            if (Request.UrlReferrer.PathAndQuery.Equals("/Login/LogoutForm"))
                return RedirectToAction("Index", "Home");
            else
                return Redirect(Request.UrlReferrer.ToString());
        }
    }
}