﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using dbwt_m3_mvc.Models;

namespace dbwt_m3_mvc.Controllers
{
    public class RegisterController : Controller
    {
        // GET: Register
        public ActionResult Index(FENutzer user) {
            return View();
        }

        public ActionResult Create() {
            return View();
        }
        
        [HttpPost]
        public ActionResult Create(FENutzer user, FormCollection form) {
            if (ModelState.IsValid) {
                if (!user.Password.Equals(user.PasswordConfirm)) {
                    ModelState.AddModelError("passwordconfirm", "Die Passwörter stimmen nicht überein");
                    return View("Create", user);
                }
                
                if (!FENutzer.CheckEMail(user.EMail)) {
                    ModelState.AddModelError("email", "Diese E-Mail-Adresse wird bereits verwendet");
                    return View("Create", user);
                }

                if (!FENutzer.CheckLoginname(user.Loginname)) {
                    ModelState.AddModelError("loginname", "Dieser Benutzername wird bereits verwendet");
                    return View("Create", user);
                }

                TempData["user"] = user;

                switch (form["role"]) {
                    case "student":
                        return RedirectToAction("CreateStudent");

                    case "mitarbeiter":
                        return RedirectToAction("CreateMitarbeiter");

                    default:
                        return RedirectToAction("CreateGast");
                }
            }
            return View("Create", user);
        }


        public ActionResult CreateStudent() {
            return View();
        }

        [HttpPost]
        public ActionResult CreateStudent(Student student) {
            if (!ModelState.IsValid)
                return View("Create", student);

            student.Save(TempData["user"] as FENutzer);
            return RedirectToAction("Index", "Home");
        }

        public ActionResult CreateMitarbeiter(){
            return View();
        }

        [HttpPost]
        public ActionResult Mitarbeiter(Mitarbeiter mitarbeiter) {
            if (!ModelState.IsValid)
                return View("Create", mitarbeiter);

            mitarbeiter.Save(TempData["user"] as FENutzer);

            return RedirectToAction("Index", "Home");
        }


        public ActionResult CreateGast(){
            return View();
        }

        [HttpPost]
        public ActionResult CreateGast(Gast gast) {
            if (!ModelState.IsValid)
                return View("Create", gast);

            gast.Save(TempData["user"] as FENutzer);
            return RedirectToAction("Index", "Home");
        }
    }
}