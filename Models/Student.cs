﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using MySql.Data.MySqlClient;

namespace dbwt_m3_mvc.Models
{
    [Bind]
    public class Student
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "Bitte Matrikelnummer eingeben")]
        public int Matrikelnummer { get; set; }
        [Required(ErrorMessage = "Bitte Studiengang eingeben")]
        public string Studiengang { get; set; }

        public int Save(FENutzer user) {
            string cs = "Server=127.0.0.1;port=3307;Database=praktikum;Uid=webapp;Pwd=webapp";
            using (MySqlConnection c = new MySqlConnection(cs)) {
                c.Open();
                MySqlTransaction tr = c.BeginTransaction();
                using (MySqlCommand cmd = c.CreateCommand()) {
                    try {
                        cmd.CommandText = "INSERT INTO `fe-nutzer` " +
                            "(vorname, nachname, loginname, email, hashedpassword) " +
                            "VALUES (@vorname, @nachname, @loginname, @email, @hashedpassword);";
                        cmd.Parameters.AddWithValue("vorname", user.Vorname);
                        cmd.Parameters.AddWithValue("nachname", user.Nachname);
                        cmd.Parameters.AddWithValue("loginname", user.Loginname);
                        cmd.Parameters.AddWithValue("email", user.EMail);
                        cmd.Parameters.AddWithValue("hashedpassword", PasswordSecurity.PasswordStorage.CreateHash(user.Password));
                        cmd.ExecuteNonQuery();

                        cmd.CommandText = "select last_insert_id();";
                        int newId = Convert.ToInt16(cmd.ExecuteScalar());
                        if (newId == -1)
                            return -1;

                        cmd.CommandText = "insert into `fh-angehörige` (id) values (@idfh);";
                        cmd.Parameters.AddWithValue("idfh", newId);
                        cmd.ExecuteNonQuery();
                    
                        cmd.CommandText = "INSERT INTO student (id, matrikelnummer, studiengang) VALUES (@id, @matrikelnummer, @studiengang);";
                        cmd.Parameters.AddWithValue("id", newId);
                        cmd.Parameters.AddWithValue("matrikelnummer", this.Matrikelnummer);
                        cmd.Parameters.AddWithValue("studiengang", this.Studiengang);
                        cmd.ExecuteNonQuery();

                        tr.Commit();
                        return newId;
                    }
                    catch {
                        tr.Rollback();
                        return -1;
                    }
                }
            }
        }
    }
}