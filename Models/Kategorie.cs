﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace dbwt_m3_mvc.Models
{
    public class Kategorie
    {
        public int ID { get; set; }
        public string Bezeichnung { get; set; }
        public int Oberkategorie { get; set; }
        public string OberkategorieBezeichnung { get; set; }
        public int FKBildId{ get; set; }

        public static List<Kategorie> GetList()
        {
            List<Kategorie> categorys = new List<Kategorie>();

            string cs = "Server=127.0.0.1;port=3307;Database=praktikum;Uid=webapp;Pwd=webapp";
            using (MySqlConnection c = new MySqlConnection(cs)) {
                c.Open();
                using (MySqlCommand cmd = c.CreateCommand()) {
                    string q = "select kat.id as 'id', kat.bezeichnung as 'bez', okat.id as 'oId',  okat.bezeichnung as 'oBez' from kategorie as kat " +
                                "inner join kategorie okat on okat.id = kat.Oberkategorie;";
                    cmd.CommandText = q;
                    var r = cmd.ExecuteReader();
                    while (r.Read()) {
                        Kategorie p = new Kategorie {
                            ID = Convert.ToInt32(r["id"]),
                            Bezeichnung = r["bez"].ToString(),
                            Oberkategorie = Convert.ToInt32(r["id"]),
                            OberkategorieBezeichnung = r["oBez"].ToString()
                        };
                        categorys.Add(p);
                    }
                }
            }
            return categorys;
        }

        public static Kategorie GetById(int _Id)
        {
            string cs = "Server=127.0.0.1;port=3307;Database=praktikum;Uid=webapp;Pwd=webapp";
            using (MySqlConnection c = new MySqlConnection(cs)) {
                c.Open();
                using (MySqlCommand cmd = c.CreateCommand()) {
                    string q = "select kat.id as 'id', kat.bezeichnung as 'bez', okat.id as 'oId',  okat.bezeichnung as 'oBez' from kategorie as kat " +
                                "inner join kategorie okat on okat.id = kat.Oberkategorie " +
                                "where kat.id = @katId;";
                    cmd.CommandText = q;
                    cmd.Parameters.AddWithValue("katId", _Id);
                    var r = cmd.ExecuteReader();
                    if (r.Read()) {
                        return new Kategorie {
                            ID = Convert.ToInt32(r["id"]),
                            Bezeichnung = r["bez"].ToString(),
                            Oberkategorie = Convert.ToInt32(r["id"]),
                            OberkategorieBezeichnung = r["oBez"].ToString()
                        };
                    }
                }
            }
            return null;
        }



        //public int Save() {
        //    string cs = "Server=127.0.0.1;port=3307;Database=praktikum;Uid=webapp;Pwd=webapp";
        //    using (MySqlConnection c = new MySqlConnection(cs)) {
        //        c.Open();
        //        MySqlTransaction tr = c.BeginTransaction();
        //        using(MySqlCommand cmd = c.CreateCommand()) {
        //            try {
        //                cmd.CommandText = "INSERT INTO kategorie (bezeichnung, oberkategorie, fkbildid) " +
        //                    "VALUES (@bezeichnung, @oberkategorie, @fkbildid);"; 
        //                cmd.Parameters.AddWithValue("bezeichnung", Bezeichnung);
        //                cmd.Parameters.AddWithValue("oberkategorie", Oberkategorie);
        //                cmd.Parameters.AddWithValue("fkbildid", FKBildId);
        //                cmd.ExecuteNonQuery();

        //                tr.Commit();
        //                cmd.CommandText = "select last_insert_id();";
        //                var newId = cmd.ExecuteScalar(); 
        //                return Convert.ToInt16(newId);
        //            }
        //            catch {
        //                tr.Rollback();
        //                return -1;
        //            }
        //        }
        //    }
        //}
    }
}