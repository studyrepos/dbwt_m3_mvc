﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace dbwt_m3_mvc.Models
{
    public class Produkt
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Beschreibung { get; set; }
        public bool Vegetarisch { get; set; }
        public bool Vegan { get; set; }
        public Dictionary<string, double> Preis { get; set; }
        public int BildID { get; set; }
        public int KategorieID { get; set; }

        public static List<Produkt> GetList(int _kategorieId = 0, int _limit = 8) {
            List<Produkt> produkte = new List<Produkt>();
            string cs = "Server=127.0.0.1;port=3307;Database=praktikum;Uid=webapp;Pwd=webapp";
            using (MySqlConnection c = new MySqlConnection(cs)){
                c.Open();
                using (MySqlCommand cmd = c.CreateCommand()) {
                    string q = "select produkt.id as 'id', name, produkt.fkbildid as 'bildId' from produkt " +
                                "join bild on produkt.fkBildId = bild.id";

                    if (_kategorieId != 0) {
                        q += " where fkkategorieid = @katId";
                        cmd.Parameters.AddWithValue("katId", _kategorieId);
                    }
                    q += " order by rand() limit @limit;";
                    cmd.CommandText = q;
                    cmd.Parameters.AddWithValue("limit", _limit);
                    var r = cmd.ExecuteReader();
                    while (r.Read()){
                        Produkt p = new Produkt {
                            ID = Convert.ToInt32(r["id"]),
                            Name = r["name"].ToString(),
                            BildID = Convert.ToInt16(r["bildId"])
                        };
                        produkte.Add(p);
                    }
                }
            }
            return produkte;
        }

        public static Produkt GetByID(int _id) {
            string cs = "Server=127.0.0.1;port=3307;Database=praktikum;Uid=webapp;Pwd=webapp";
            using (MySqlConnection c = new MySqlConnection(cs)) {
                c.Open();
                using (MySqlCommand cmd = c.CreateCommand()) {
                    string q = "select name, beschreibung, fkbildid, gastbetrag, mitarbeiterbetrag, studentbetrag from produkt " +
                                "join preis on produkt.fkPreisId = preis.id " +
                                "join bild on produkt.fkBildId = bild.id " +
                                "where produkt.id = @id " +
                                "limit 1;";
                    cmd.CommandText = q;
                    cmd.Parameters.AddWithValue("id", _id);
                    var r = cmd.ExecuteReader();
                    if (r.Read()) {
                        return new Produkt {
                            Name = r["name"].ToString(),
                            Beschreibung = r["beschreibung"].ToString(),
                            BildID = Convert.ToInt16(r["fkbildid"]),
                            Preis = new Dictionary<string, double>() {
                                {  "gastbetrag", Convert.ToDouble(r["gastbetrag"]) },
                                {  "studentbetrag", Convert.ToDouble(r["studentbetrag"]) },
                                {  "mitarbeiterbetrag", Convert.ToDouble(r["mitarbeiterbetrag"]) }
                            }
                        };
                    }
                }
            }
            return null;
        }


        //public int Save() {
        //    string cs = "Server=127.0.0.1;port=3307;Database=praktikum;Uid=webapp;Pwd=webapp";
        //    using (MySqlConnection c = new MySqlConnection(cs)) {
        //        c.Open();
        //        MySqlTransaction tr = c.BeginTransaction();
        //        using(MySqlCommand cmd = c.CreateCommand()) {
        //            try {
        //                cmd.CommandText = "INSERT INTO Produkt (NutzerID) VALUES (@nutzerID);"; 
        //                cmd.Parameters.AddWithValue("nutzerId", NutzerID);
        //                cmd.ExecuteNonQuery();

        //                tr.Commit();
        //                cmd.CommandText = "select last_insert_id();";
        //                var newId = cmd.ExecuteScalar(); 
        //                return Convert.ToInt16(newId);
        //            }
        //            catch {
        //                tr.Rollback();
        //                return -1;
        //            }
        //        }
        //    }
        //}
    }
}