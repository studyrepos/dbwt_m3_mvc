﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace dbwt_m3_mvc.Models
{
    public class Bild
    {
        public int ID { get; set; }

        public int Save() {
            string cs = "Server=127.0.0.1;port=3307;Database=praktikum;Uid=webapp;Pwd=webapp";
            using (MySqlConnection c = new MySqlConnection(cs)) {
                c.Open();
                MySqlTransaction tr = c.BeginTransaction();
                using(MySqlCommand cmd = c.CreateCommand()) {
                    try {
                        cmd.CommandText = "INSERT INTO bild (ID) VALUES (@ID);"; 
                        cmd.Parameters.AddWithValue("ID", ID);
                        cmd.ExecuteNonQuery();

                        tr.Commit();
                        cmd.CommandText = "select last_insert_id();";
                        var newId = cmd.ExecuteScalar(); 
                        return Convert.ToInt16(newId);
                    }
                    catch {
                        tr.Rollback();
                        return -1;
                    }
                }
            }
        }
    }
}