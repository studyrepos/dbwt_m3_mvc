﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using MySql.Data.MySqlClient;

namespace dbwt_m3_mvc.Models
{
    [Bind]
    public class FENutzer
    {
        public int Nr { get; set; }

        [Required(ErrorMessage = "Bitte Vornamen eingeben")]
        public string Vorname { get; set; }
        [Required(ErrorMessage = "Bitte einen Nachnamen eingeben")]
        public string Nachname { get; set; }
        [Required(ErrorMessage = "Bitte Login-Namen eingeben")]
        public string Loginname { get; set; }
        [Required(ErrorMessage = "Bitte E-Mail-Adresse eingeben")]
        public string EMail { get; set; }

        [Required(ErrorMessage = "Bitte Passwort eingeben")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Bitte Passwort wiederholen")]
        public string PasswordConfirm { get; set; }

        public char HashedPassword { get; set; }
        public bool Aktiv { get; set; }
        public DateTime Anlegedatum { get; set; }
        public DateTime LetzterLogin { get; set; }
        public string Role { get; set; }

        public static FENutzer GetByCredentials(string _loginname, string _password) {
            string cs = "Server=127.0.0.1;port=3307;Database=praktikum;Uid=webapp;Pwd=webapp";
            using (MySqlConnection c = new MySqlConnection(cs)) {
                c.Open();
                using (MySqlCommand cmd = c.CreateCommand()) {
                    string q = "select nr, loginname, vorname, hashedpassword, aktiv, letzterLogin from `fe-nutzer` " +
                                "where loginname = @loginname;";
                    cmd.CommandText = q;
                    cmd.Parameters.AddWithValue("loginname", _loginname);

                    var r = cmd.ExecuteReader();
                    if (r.Read()) {
                        if (PasswordSecurity.PasswordStorage.VerifyPassword(
                                _password, r["hashedpassword"].ToString()) && Convert.ToInt16(r["aktiv"]) == 1) {
                            FENutzer user = new FENutzer {
                                Nr = Convert.ToInt32(r["nr"]),
                                Vorname = r["vorname"].ToString(),
                                Loginname = r["loginname"].ToString()
                            };
                            if (DateTime.TryParse(r["letzterLogin"].ToString(), out DateTime lastLogin))
                                user.LetzterLogin = lastLogin;

                            r.Close();

                            cmd.CommandText = "update `fe-nutzer` set `LetzterLogin` = @now where  nr = @uid;";
                            cmd.Parameters.AddWithValue("now", DateTime.Now);
                            cmd.Parameters.AddWithValue("uid", user.Nr);
                            cmd.ExecuteNonQuery();
                            r.Close();

                            cmd.CommandText = "select id from mitarbeiter where id = @mid;";
                            cmd.Parameters.AddWithValue("mid", user.Nr);
                            r = cmd.ExecuteReader();
                            if (r.Read())
                                user.Role = "mitarbeiter";
                            else {
                                r.Close();
                                cmd.CommandText = "select id from student where id = @sid;";
                                cmd.Parameters.AddWithValue("sid", user.Nr);
                                if (cmd.ExecuteReader().HasRows)
                                    user.Role = "student";
                                else
                                    user.Role = "gast";
                                r.Close();
                            }
                            return user;
                        }
                    }
                }
            }
            return null;
        }
        
        public static bool CheckEMail(string _email) {
            string cs = "Server=127.0.0.1;port=3307;Database=praktikum;Uid=webapp;Pwd=webapp";
            using (MySqlConnection c = new MySqlConnection(cs)) {
                c.Open();
                using (MySqlCommand cmd = c.CreateCommand()) {
                    cmd.CommandText = "select email from `fe-nutzer` " +
                                "where email = @email;";
                    cmd.Parameters.AddWithValue("email", _email);
                    if (!cmd.ExecuteReader().Read())
                        return true;
                    }
            }
            return false;
        }

        public static bool CheckLoginname(string _loginname)
        {
            string cs = "Server=127.0.0.1;port=3307;Database=praktikum;Uid=webapp;Pwd=webapp";
            using (MySqlConnection c = new MySqlConnection(cs)) {
                c.Open();
                using (MySqlCommand cmd = c.CreateCommand()) {
                    cmd.CommandText = "select loginname from `fe-nutzer` " +
                                "where loginname = @loginname;";
                    cmd.Parameters.AddWithValue("loginname", _loginname);
                    if (!cmd.ExecuteReader().Read())
                        return true;
                }
            }
            return false;
        }


        public int Save()
        {
            string cs = "Server=127.0.0.1;port=3307;Database=praktikum;Uid=webapp;Pwd=webapp";
            using (MySqlConnection c = new MySqlConnection(cs)) {
                c.Open();
                using (MySqlCommand cmd = c.CreateCommand()) {
                    try {
                        cmd.CommandText = "INSERT INTO `fe-nutzer` " +
                            "(vorname, nachname, loginname, email, hashedpassword) " +
                            "VALUES (@vorname, @nachname, @loginname, @email, @hashedpassword);";

                        cmd.Parameters.AddWithValue("vorname", this.Vorname);
                        cmd.Parameters.AddWithValue("nachname", this.Nachname);
                        cmd.Parameters.AddWithValue("loginname", this.Loginname);
                        cmd.Parameters.AddWithValue("email", this.EMail);
                        cmd.Parameters.AddWithValue("hashedpassword", PasswordSecurity.PasswordStorage.CreateHash(this.Password));

                        cmd.ExecuteNonQuery();

                        cmd.CommandText = "select last_insert_id();";
                        var newId = cmd.ExecuteScalar();
                        return Convert.ToInt16(newId);
                    }
                    catch {
                        return -1;
                    }
                }
            }
        }
    }
}